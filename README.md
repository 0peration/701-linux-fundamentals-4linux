# 701 Linux Fundamentals - 4linux
## https://4linux.com.br/cursos/treinamento/linux-fundamentals/

## Aula 01 - Bem vindo ao Maravilhoso mundo do Linux
    1.1: Mercado Linux
    1.2: Certificação Linux
    1.3: História do Linux
    1.4: Licenças OpenSource
    1.5: Evolução do Linux: Distribuições/Dispositivos embarcados
    1.6: Conhecendo o Linux e suas principais Aplicações
    1.7: Tópicos para Revisão do capitulo

## Aula 02 - Primeiros passos no Linux
    2.1: Infraestrutura do curso e componentes básicos de uma rede
    2.2: Terminais Virtuais
    2.3: Execução dos primeiros comandos
    2.4: Shell e Variáveis Locais e de Ambiente(Globais)
    2.5: Arquivos de Configuração do Shell 
    2.6: Caminhos de diretórios
    2.7: Tópicos para Revisão do capitulo

## Aula 03 - Obtendo ajuda no Linux
    3.1: Formas de Documentação
    3.2: Comandos Help, Apropos e whatis
    3.3: Comandos Man
    3.4: Comandos info, whereis e which
    3.5: Tópicos para Revisão do capitulo

## Aula 04 - Filesystem Hierarchy Standard e Comandos Indispensáveis
    4.1: FHS, Hierarquia dos diretórios
    4.2: Definição dos diretórios
    4.3: Aprendendo Comandos do GNU/Linux
    4.4: Localização no sistema - Find e Locate
    4.5: Tópicos para Revisão do capitulo

## Cookbooks - Revisão e Prática
    Laboratório 1
    Laboratório 2
    Laboratório 3
